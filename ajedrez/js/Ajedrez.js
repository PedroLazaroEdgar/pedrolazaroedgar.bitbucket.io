 
 var Ajedrez=(function(){
   
  var descargar=function(){
    var tablero= document.getElementById("tablero");
    tablero.innerHTML='';
    var table = document.createElement("table");
    table.id="tabla";
    var url = "https://pedrolazaroedgar.bitbucket.io/ajedrez/csv/tablero.csv";
    var xhr = new XMLHttpRequest();		
	xhr.open('GET', url, true);
	xhr.onreadystatechange = function (){
	  
			if(xhr.readyState === 4)
			{
				if(xhr.status === 200)
				{
				  var data = xhr.responseText;
				  var allRows = data.split(/\r?\n|\r/);	
				  //var fila=table.insertRow();
				  //var d=allRows[0].split('|');
				  
				  for(i=0;i<9;i++){
				    
				    fila=table.insertRow();
				    d=allRows[i].split('|');
				  for(j=0;j<8;j++){
				    
				    celda=fila.insertCell();
				    celda.innerHTML =d[j];
				    
				  }
				    celda=fila.insertCell();
				    celda.innerHTML=i;
				  }
				  
				  tablero.appendChild(table);
				}
			}
		}
		xhr.send(null);
  };
  var _mostrar_tablero=function(){
    var opcion=document.getElementById("opcion");
    var boton=document.createElement("BUTTON");
    boton.innerHTML="Actualizar";
    boton.addEventListener("click",_actualizar_tablero);
    opcion.appendChild(boton);
   descargar();
  };
  
  var _actualizar_tablero=function(){
    descargar();
  };
  
  var _mover_pieza=function(de ,posicion_actual,a,posicion_final){
    mensaje=document.getElementById("mensaje");
    tablero=document.getElementById("tabla")
    var columna="abcdefgh";
    var fila=" 12345678";
    var _fila=fila.indexOf(posicion_actual.charAt(1));
    var _columna=columna.indexOf(posicion_actual.charAt(0));
    if(_fila>0&&_columna>0){
    var valor=tablero.rows[_fila].cells[_columna].textContent;
    tablero.rows[_fila].cells[_columna].textContent=" "; 
    }
    else
     mensaje.innerHTML="A ocurrido un error"
     
    var _fil=fila.indexOf(posicion_final.charAt(1));
    var _column=columna.indexOf(posicion_final.charAt(0));
    
    if(_fila>0&&_columna>0)
    tablero.rows[_fil].cells[_column].textContent=valor;
    else
     mensaje.innerHTML="A ocurrido un error"
  };
  return {
    "mostrarTablero": _mostrar_tablero,
    "actualizarTablero":_actualizar_tablero,
    "moverPieza":_mover_pieza
  }
})();